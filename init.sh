#!/bin/bash

git submodule init

git submodule update

cd backend

git checkout develop

git pull

npm install

cd ..

docker-compose up --build
