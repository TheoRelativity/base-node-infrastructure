# Base Node Infrastructure

A node backend linked to a mysql database.

## First installation

Add your repo as a submodule

`$ git submodule add < git repo URL > services/backend`

Run the first installation

`$ ./init.sh`

## Start the infrastructure

Once you ran the first installation you can run the infrastructure executing this command

`$ ./run.sh`

## Stop the infrastructure

`$ ./stop.sh`

## Restart the infrastructure

`$ ./restart.sh`


## Read logs

Show the backend terminal

`$ docker logs -f vps_inf_backend`

Show the mysql terminal

`$ docker logs -f vps_inf_mysql`

## Interact with the docker container

Interact with the backend infrastructure

`$ docker-exec vps_inf_backend bash`

Interact with the mysql infrastructure

`$ docker-exec vps_inf_mysql`
